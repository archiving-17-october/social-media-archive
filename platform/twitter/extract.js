// twitter id -> { data: { ... }, media: { images, video} } 
const moment = require('moment-timezone');
const he = require("he")
const request = require("../../util/request")
const auth = require("./authentication")
const { extractUrls, extractHashtags, extractMentions } = require('twitter-text')

function queryString(params) {
    return Object.keys(params).map(key => key + '=' + params[key]).join('&');
}

async function extract(id) {
    const authOptions = {
        headers: {
            authorization: `Bearer ${auth.BEARER_TOKEN}`,
            'x-csrf-token': 'undefined',
            'x-guest-token': await auth.getGuestToken(),
            'x-twitter-client-language': 'en',
            ...auth.USER_AGENT_HEADER
        }
    }

    let params = queryString({
            tweet_mode: 'extended',
            include_reply_count: 1,
            simple_quoted_tweet: true,
            include_quote_count: true,
            include_cards: 1,
            cards_platform: 'Web-12',
        });
    
    let response = await request.json(`https://api.twitter.com/2/timeline/conversation/${id}.json?${params}`, authOptions) 
    
    if(response.errors) {
        if(response.errors[0].code === 88) {
            console.log('rate limit! refreshing guest token', id, response.errors);
            await auth.invalidateGuestToken()
            return extract(id)
        }
        else {
            console.log('error! skipping', id, response.errors);
            return null;
        }
    }

    const tweet = response.globalObjects.tweets[id];
    
    if (!tweet) return null;

    let userId = tweet.user_id_str,
        user = response.globalObjects.users[userId],
        text = he.decode(tweet.full_text),
        tags = extractHashtags(text),
        mentions = extractMentions(text),
        inlineLinks = extractUrls(text),
        timestamp = moment.utc(new Date(tweet.created_at)).format(),
        links = { external: [], internal: [] },
        thread = { prev: null, next: null },
        images = [],
        video = null,
        location = tweet.place ? tweet.place.full_name : null,
        conversation = Object.keys(response.globalObjects.tweets)

    if (inlineLinks) {
        for (const link of inlineLinks) {
            let expandedLink = await request.expand(link)
            // TODO is dropping trailing links overthinking it?
            if (text.endsWith(link))
                text = text.replace(link, "")
            else
                text = text.replace(link, expandedLink)
            if (expandedLink.startsWith("https://twitter.com")) {
                let statusMatch = expandedLink.match(/\/status\/([\d]+)/)
                if (statusMatch) {
                    let tweetId = statusMatch[1]
                    if (!links.internal.includes(tweetId) && tweetId != id)
                        links.internal.push(tweetId)
                } else {
                    let broadcastMatch = expandedLink.match(/\/broadcasts\/([^\/]+)/)
                    if (broadcastMatch) {
                        let broadcastId = broadcastMatch[1]
                        video = { type: "twitter-live-video", id: broadcastId }
                    } else {
                        if (!links.external.includes(expandedLink))
                            links.external.push(expandedLink)
                    }
                }
            } else if (expandedLink.startsWith("https://www.pscp.tv") && expandedLink.match(/\/\w+\/([^\/]+)/)) {
                let periscopeId = expandedLink.match(/\/\w+\/([^\/]+)/)[1]
                video = { type: "periscope-video", id: periscopeId }
            } else {
                if (!links.external.includes(expandedLink))
                    links.external.push(expandedLink)
            }
        }
    }

    if(tweet.extended_entities)
        for (const m of tweet.extended_entities.media) {
            if(m.type == 'video' || m.type == 'animated_gif')
                video = { type: 'twitter-video', id: tweet.id_str }
            if(m.type == 'photo')
                images.push(m.media_url_https)
        }

    text = text.trim()

    if (tweet.in_reply_to_status_id_str)
        thread.prev = tweet.in_reply_to_status_id_str;
    
    for (const o of Object.values(response.globalObjects.tweets)) {
        if(o.in_reply_to_status_id_str == tweet.id_str) {
            thread.next = o.id_str
            break;
        }
    }

    if (tweet.quoted_status_id_str)
        links.internal.push(tweet.quoted_status_id_str);
    
    return {
        user: user.screen_name,
        likes: tweet.favorite_count,
        replies: tweet.reply_count,
        retweets: tweet.retweet_count,
        id, location, thread, links, text, tags,
        mentions, timestamp, video, images, conversation
    }
}

// (async () => {
//     // 1270987424139706369
//     // console.log(await _fetchTweet("1270987424139706369"))
//     // console.log(await _fetchTweet("1270789433319723010"))
//     // console.log(await _fetchTweet("1270753801679626240"))
//     // twitter video
//     // console.log(await _fetchTweet("1271921003074109441"))
//     // pscp
//     // console.log(await _fetchTweet("1271822272592875522"))
//     // twitter video
//     // console.log(await _fetchTweet("1256384900141326346"))
//     // single photo
//     // console.log(await _fetchTweet("1272031711652491265"))
//     // two photos
//     // console.log(await _fetchTweet("1271760531439980545"))
//     // gif
//     // console.log(await _fetchTweet("1270768531890716673"))
//     // link
//     // console.log(await _fetchTweet("1271773047540908038"))
//     // location
//     // console.log(await _fetchTweet("1267662994797998080"))
// })()

module.exports = extract