const moment = require("moment")

async function idsFromTwitterSearch(browser, url, cb) {
    const page = await browser.newPage();
    // spoof ie6
    await page.setUserAgent('Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');
    
    while(url) {
        let ids = [];
        await page.goto(url);
        [ids, url] = await page.evaluate(_ => {
            let ids = Array.from(document.querySelectorAll("td.timestamp a")).map(a => a.getAttribute("name").match(/\d+$/)[0])
            let olderTweetsButton = document.querySelector(".w-button-more a")
            let url = olderTweetsButton ? olderTweetsButton.getAttribute("href") : null;
            return [ids, url];
        })
        if(ids.length > 0)
            await cb(ids)
        if(url)
            url = `https://twitter.com${url}`
    }
    await page.close()
}

async function idsFromHashtag(browser, hashtag, date, cb) {
    let m = moment(date)
    let since = m.format("Y-MM-DD")
    let until = m.add(1, 'days').format("Y-MM-DD")
    let url = `https://twitter.com/search?q=%23${hashtag}%20since%3A${since}%20until%3A${until}&f=live`

    await idsFromTwitterSearch(browser, url, cb)   
}

async function idsFromAccount(browser, account, date, cb) {
    let m = moment(date)
    let since = m.format("Y-MM-DD")
    let until = m.add(1, 'days').format("Y-MM-DD")
    let url = `https://twitter.com/search?q=from%3A${account}%20since%3A${since}%20until%3A${until}&f=live`

    await idsFromTwitterSearch(browser, url, cb)   
}

module.exports = { idsFromHashtag, idsFromAccount }