const request = require("../../util/request")
const { osxChromeUserAgent } = require("../../common")

// Twitter authentication
const BEARER_TOKEN = 'AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA';
const USER_AGENT_HEADER = { 'User-Agent': osxChromeUserAgent };

let _cachedGuestToken = null;

// Get guest token to access Twitter API
async function getGuestToken(bearerToken = BEARER_TOKEN) {
    if(_cachedGuestToken === null)
        _cachedGuestToken = await requestGuestToken(bearerToken);
    return _cachedGuestToken;
}

function invalidateGuestToken() {
    _cachedGuestToken = null;
}

async function requestGuestToken(token) {
    let proxy = `http://${await request.randomProxy()}`
    console.log('[twitter auth]', 'guest token: requesting, proxy', proxy)
    try {
        const guestTokenResponse = await request('https://api.twitter.com/1.1/guest/activate.json',
            {
                method: 'POST',
                headers: { authorization: `Bearer ${token}`, USER_AGENT_HEADER },
                timeout: 3000,
                proxy
            })
        let guestToken = JSON.parse(guestTokenResponse.body).guest_token;
        console.log('[twitter auth]', 'guest token: got', guestToken)
        return guestToken
    } catch(e) {
        console.log('[twitter auth]', 'error', e);
        return requestGuestToken(token);
    }
}

module.exports = { getGuestToken, invalidateGuestToken, BEARER_TOKEN, USER_AGENT_HEADER }