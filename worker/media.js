const { queues } = require('../config/queues')
const os = require('os')
const cpuCount = os.cpus().length

const twitterVideo = require('../platform/twitter/video')
const { store, transcodeAndStore, keyExists } = require("../storage/s3")

// 1 node instance / cpu
const CONCURRENCY = cpuCount

async function storeMedia(job) {
    try {
        if(await keyExists(job.data.destination)) {
            console.log(job.data.destination, "already exists, skipping");
            return;
        }
        
        let m3u8Url;
        switch (job.data.strategy) {
            case "file":
                await store(job.data.url, job.data.destination, job.data.type)
                break;

            case "twitter-video":
                video = await twitterVideo.videoFromTweet(job.data.id)
                if(video === null) {
                    console.log(job.data.id, "missing, skipping");
                    return;
                }
                switch(video.type) {
                    case 'video/mp4':
                        await store(video.url, job.data.destination, "video/mp4")
                        break;
                    case 'application/x-mpegURL':
                        await transcodeAndStore(video.url, job.data.destination, "video/mp4", job)
                        break;
                    default:
                        throw new Error(`unexpected video type ${video.type}`)
                }
                break;

            case "twitter-live-video":
                m3u8Url = await twitterVideo.m3u8FromTwitterBroadcast(job.data.id)
                if(!m3u8Url) {
                    throw new Error(`null m3u8 url for twitter-live-video ${job.data.id}\n${JSON.stringify(job.data)}`)
                }
                await transcodeAndStore(m3u8Url, job.data.destination, "video/mp4", job)
                break;
            case "periscope-video":
                m3u8Url = await twitterVideo.m3u8FromPeriscope(job.data.id)
                if(!m3u8Url) {
                    console.log(`null m3u8 url for periscope-video ${job.data.id}, broadcast ended or deleted, skipping`)
                    return;
                }
                await transcodeAndStore(m3u8Url, job.data.destination, "video/mp4", job)
                break;

            default:
                if (job.data.strategy) throw new Error(`unsupported media strategy ${job.data.strategy}`)
                else throw new Error(`no media strategy`)
        }
    } catch(e) {
        console.log(e);
        job.moveToFailed(e)
    }
}

module.exports = { storeMedia }

if (require.main === module) {
    queues.media.process(CONCURRENCY, storeMedia)
    console.log(`starting to store media with concurrency ${CONCURRENCY}`);
}