const { queues, enqueue } = require('../config/queues');
const puppeteer = require('puppeteer');
const cluster = require("cluster");
const db = require("../storage/db");
const os = require('os')
const cpuCount = os.cpus().length
const moment = require('moment');
const request = require('request');
const fs = require('fs');
const { randomFileName, timeout } = require('../common')
const XLSX = require('xlsx');

const twitterGather = require("../platform/twitter/gather")
// TODO instagram gather

// 2 node instance / CPU
const WORKERS = cpuCount * 2
// 8 tasks / node instance
const CONCURRENCY = 8

let sharedBrowser;

function downloadFile(url, out) {
    let outStream = fs.createWriteStream(out)
    return new Promise((resolve, reject) => {
        let stream = request(url).pipe(outStream);
        stream.on('error', reject)
        stream.on('finish', _ => resolve())
    })
}

let gatherJobHandlers = {
    // { url: "..." }
    'lebanonprotests.com-xlsx-import': async job => {
        let filename = randomFileName()
        console.log('downloading', job.url);
        await downloadFile(job.url, filename)
        console.log('finished', job.url);
        var workbook = XLSX.readFile(filename);
        for (const k in workbook.Sheets.Tweets) {
            if (workbook.Sheets.Tweets.hasOwnProperty(k) && k.startsWith('F')) {
                const cell = workbook.Sheets.Tweets[k];
                if (cell.t === 's') {
                    let m = cell.v.match(/\d+$/)
                    if (m) {
                        console.log('push', m);
                        await enqueue(queues.extract,
                                      { platform: "twitter.com",
                                        id: m[0],
                                        origin: job.data })
                    }
                }
            }
        }
        fs.unlinkSync(filename)
    },
    // { hashtag: "..." }
    'twitter-hashtag-recent': async job => {
        let today = moment();
        let d = moment().subtract(2, 'days');
        while (today.diff(d, 'days') >= 0) {
            await enqueue(queues.gather, ({ type: "twitter-hashtag-on-date", hashtag: job.data.hashtag, date: d.format('YYYY-MM-DD'), origin:job.data }))
            d.add(1, 'day')
        }
    },
    // { hashtag: "..." }
    'twitter-hashtag-backlog': async job => {
        let start = moment('2019-06-03');
        let d = moment();
        while (d.diff(start, 'days') >= 0) {
            let j = { type: "twitter-hashtag-on-date", hashtag: job.data.hashtag, date: d.format('YYYY-MM-DD'), origin:job.data }
            await enqueue(queues.gather, j)
            d.subtract(1, "day")
        }
    },
    // { account: "..." }
    'twitter-account-backlog': async job => {
        let start = moment('2019-06-03');
        let d = moment();
        while (d.diff(start, 'days') >= 0) {
            let j = { type: "twitter-account-on-date", account: job.data.account, date: d.format('YYYY-MM-DD'), origin:job.data }
            await enqueue(queues.gather, j)
            d.subtract(1, "day")
        }
    },
    // { hashtag: "...", date:"YYYY-MM-DD" }
    'twitter-hashtag-on-date': async job => {
        await twitterGather.idsFromHashtag(sharedBrowser, job.data.hashtag, job.data.date, async ids => {
            let newIds = await db.newIds("twitter.com", ids)
            for (const { id } of newIds)
                await enqueue(queues.extract, { platform: "twitter.com", id, origin:job.data })
        })
    },
    // { account: "...", date:"YYYY-MM-DD" }
    'twitter-account-on-date': async job => {
        await twitterGather.idsFromAccount(sharedBrowser, job.data.account, job.data.date, async ids => {
            // console.log("await newIds", ids)
            let newIds = await db.newIds("twitter.com", ids)
            console.log("finished waiting", ids)
            for (const { id } of newIds)
                await enqueue(queues.extract, { platform: "twitter.com", id, origin:job.data })
        })
    },
    // { hashtag: "..." }
    // 'instagram-hashtag': async job => {
    //     await instagramGather.gather(job.data.hashtag, job, async newIds => {
    //         for (const { id } of newIds)
    //             await enqueue(queues.extract, { platform: "instagram.com", id, origin:job.data })
    //     })
    // }
}

async function cleanUp() {
    if(sharedBrowser) {
        console.log('killing shared browser');
        await sharedBrowser.close(); // cant await, must be synchronus
    }
}

process.on('exit', cleanUp);
process.on('SIGINT', cleanUp);
process.on('SIGUSR1', cleanUp);
process.on('SIGUSR2', cleanUp);

if (cluster.isMaster) {
    for (let i = 0; i < WORKERS; i++)
        cluster.fork()

} else {
    (async () => {
        sharedBrowser = process.env.NODE_ENV === 'production' ?
            await puppeteer.launch({ headless: true  }) :
            await puppeteer.launch({ headless: false });
        
        queues.gather.process(CONCURRENCY, async job => {
            let handler = gatherJobHandlers[job.data.type]
            if (handler)
                return await handler(job)
            throw `unsupported gather type ${job.data.type}`
        })

        console.log(`starting to gather with concurrency ${CONCURRENCY}`);
    })()
}