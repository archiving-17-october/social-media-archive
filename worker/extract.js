const { queues, enqueue } = require('../config/queues');
const cluster = require("cluster");
const os = require('os')
const cpuCount = os.cpus().length

const twitterExtract = require("../platform/twitter/extract")
const instagramExtract = require("../platform/instagram/extract")

// 2 node instance / CPU
const WORKERS = cpuCount
// 8 tasks / node instance
const CONCURRENCY = 8

async function commitMissingRecord(data) {
    await enqueue(queues.commit, { table: 'missing', data })
}

async function commitRecord(data) {
    console.log('[extract] commit record', data.id);
    await enqueue(queues.commit, { table: 'record', data })
}

async function commitMedia(data) {
    console.log('[extract] commit media', data[0].id);
    await enqueue(queues.commit, { table: 'media', data })
}

async function extract(job) {
    let data, record, mediaIndex, mediaRows;

    switch (job.data.platform) {
        // { platform:"instagram.com"|"twitter.com", id:"...", }
        case "twitter.com":
            data = await twitterExtract(job.data.id)
            if(!data) {
                // data is undefined, tweet is most likely deleted or account
                // was suspended. record in missing table.
                await commitMissingRecord({ platform: 'twitter.com', id: job.data.id, origin: job.data.origin })
                return;
            }

            record = {
                platform: 'twitter.com',
                id: data.id,
                original_time: data.timestamp,
                author: data.user,
                text: data.text,
                original_url: `https://twitter.com/${data.user}/status/${data.id}`,
                location: data.location,
                tags: data.tags,
                mentions: data.mentions,
                likes: data.likes,
                responses: data.replies,
                extra: {
                    retweets: data.retweets,
                    internal_links: data.links.internal,
                    external_links: data.links.external,
                    thread: data.thread,
                    poll: data.poll
                },
                origin: job.data.origin
            }

            await commitRecord(record)

            mediaIndex = 0;
            mediaRows = []
            for (const url of data.images) {
                let destination = `media/twitter.com/${data.id}/${mediaIndex++}.jpg`
                mediaRows.push({ platform: 'twitter.com', id: data.id, key: destination, type: 'image/jpeg', })
                await enqueue(queues.media, { strategy: 'file', type: 'image/jpeg', url, destination })
            }

            if (data.video) {
                let destination = `media/twitter.com/${data.id}/${mediaIndex++}.mp4`
                mediaRows.push({ platform: 'twitter.com', id: data.id, key: destination, type: 'video/mp4', })
                await enqueue(queues.media, { strategy: data.video.type, id: data.video.id, type: 'video/mp4', destination })
            }

            if(mediaRows.length > 0)
                await commitMedia(mediaRows)

            break;

        case "instagram.com":
            data = await instagramExtract(job.data.id)
            break;

        default:
            if (job.data.type) throw new Error(`unsupported gather type ${job.data.type}`)
            else throw new Error(`no type property in gather job`)
    }
}

(async () => {
    if (cluster.isMaster) {
        if(WORKERS===1&&CONCURRENCY===1) {
            console.log(`starting to extract without concurrency`);
            queues.extract.process(CONCURRENCY, extract)
        } else {
            for (let i = 0; i < WORKERS; i++)
                cluster.fork()
        }

    } else {
        queues.extract.process(CONCURRENCY, extract)
        console.log(`starting to extract with concurrency ${CONCURRENCY}`);
    }

})()