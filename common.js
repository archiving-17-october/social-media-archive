function timeout(time) {
    return new Promise((resolve) => {
        setTimeout(resolve, time)
    })
}

function randomFileName() {
    return Math.random().toString(36).replace("0.", Date.now().toString(36))
}

function filterMax(array, fn) {
    return array.reduce((prev, current) =>
        (fn(prev) > fn(current)) ? prev : current)
}

const osxChromeUserAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.79 Safari/537.36'

module.exports = { randomFileName, osxChromeUserAgent, filterMax, timeout }