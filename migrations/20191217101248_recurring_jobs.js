exports.up = async function(knex) {
    await knex.schema.createTable("work_recurring", table => {
        table.increments('id')
        table.timestamp('started_timestamp').notNullable().defaultTo(knex.raw("'now'"))
        table.text('started_user').notNullable()
        table.jsonb('job').notNullable()
    })
};

exports.down = async function(knex) {
    await knex.schema.dropTableIfExists("work_recurring")
};