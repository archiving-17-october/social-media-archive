exports.up = async function(knex) {
    await knex.schema.alterTable('record', table => {
        table.timestamp('archive_time').nullable().defaultTo(knex.fn.now()).alter()
    })
};

exports.down = async function(knex) {
    await knex.schema.alterTable('record', table => {
        table.timestamp('archive_time').notNullable().defaultTo(knex.raw("'now'")).alter()
    })  
};