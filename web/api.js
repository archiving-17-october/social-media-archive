const queues = require('../config/queues')
const db = require('../storage/db')
const router = require('express').Router()

// TODO put this in one place
let mediaUrl = media => `https://arsheev.s3.amazonaws.com/${media.key}`

router.get('/v1/records/:platform/:id.json', async (req, res) => {
    let data = await db.getRecord(req.params.platform, req.params.id)
    data.media = (await db.getMedia(req.params.platform, req.params.id)).map(m => {
        return { type: m.type, url: mediaUrl(m) }
    })
    res.type('application/json')
    res.send(JSON.stringify(data))
})

router.get('/v1/work/status', async (req, res) => {
    res.type('application/json')
    res.send(JSON.stringify(await queues.status()))
})

module.exports = router