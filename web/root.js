const commaNumber = require('comma-number')
const db = require('../storage/db')
const s3 = require('../storage/s3')
const queues = require('../config/queues')
const router = require('express').Router({mergeParams:true})

// https://stackoverflow.com/a/18650828
function formatBytes(bytes, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

router.get('/status', async (req, res) => {
    let records = commaNumber((await db.getAllRecords().count().first()).count)
    let missingRecords = commaNumber((await db.getAllMissingRecords().count().first()).count)
    let mediaFiles = commaNumber((await db.getAllMedia().count().first()).count)
    let mediaSize = formatBytes(await s3.getSpaceUsed())
    let queueStatus = await queues.status()
    let gatherJobs = queueStatus.pending.gather
    let extractJobs = queueStatus.pending.extract
    let mediaJobs = queueStatus.pending.media
    
    let recurringWork = await db.getRecurringWork();
    let recurringHashtagBacklogJobs = recurringWork.filter(w => w.job.type === 'twitter-hashtag-backlog')
    let recurringAccountBacklogJobs = recurringWork.filter(w => w.job.type === 'twitter-account-backlog')
    
    res.render(`status`, { ...req.d12n, records, missingRecords, mediaFiles, mediaSize, gatherJobs, extractJobs, mediaJobs, recurringHashtagBacklogJobs, recurringAccountBacklogJobs })
})

router.get('/', async (req, res) => {
    res.render('index', { ...req.d12n })
})

module.exports = router