const bullBoard = require('bull-board')
const { queues } = require("../../config/queues")
const router = require('express').Router()

bullBoard.setQueues(Object.values(queues))

router.use('/queues', bullBoard.UI)

module.exports = router