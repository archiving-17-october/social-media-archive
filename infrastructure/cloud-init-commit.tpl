#cloud-config
runcmd:
  - curl -sL https://deb.nodesource.com/setup_13.x | bash
  - apt install -y nodejs
  - [ cd, /home/ubuntu ]
  - [ git, clone, "${git_repo}" ]
  - [ cd, social-media-archive ]
  - [ git, checkout, "${git_commit}" ]
  - [ npm, install ]
  - [ systemctl, enable, --now, arsheev ]
write_files:
  - path: /etc/systemd/system/arsheev.service
    owner: root:root
    permissions: '0755'
    content: |
        [Unit]
        Description=Start Arsheev Worker
        Wants=network-online.target
        After=network-online.target

        [Service]
        User=ubuntu
        Type=simple
        Environment=NODE_ENV=production
        Environment=REDISGREEN_URL=${redis_url}
        Environment=HEROKU_POSTGRESQL_OLIVE_URL=${postgres_url}
        Environment=AWS_ACCESS_KEY_ID=${aws_access_key_id}
        Environment=AWS_SECRET_ACCESS_KEY=${aws_secret_access_key}
        Environment=HALT_ON_EMPTY_QUEUE=true
        WorkingDirectory=/home/ubuntu/social-media-archive
        StandardOutput=file:/var/log/arsheev-${worker}.out
        StandardError=file:/var/log/arsheev-${worker}.err
        ExecStart=/usr/bin/node /home/ubuntu/social-media-archive/worker/${worker}.js
        Restart=on-failure
        RestartSec=15

        [Install]
        WantedBy=multi-user.target