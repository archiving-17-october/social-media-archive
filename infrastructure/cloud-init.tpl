#cloud-config
packages:
  - ffmpeg
  - ca-certificates
  - fonts-liberation
  - libappindicator3-1
  - libasound2
  - libatk-bridge2.0-0
  - libatk1.0-0
  - libc6
  - libcairo2
  - libcups2
  - libdbus-1-3
  - libexpat1
  - libfontconfig1
  - libgbm1
  - libgcc1
  - libglib2.0-0
  - libgtk-3-0
  - libnspr4
  - libnss3
  - libpango-1.0-0
  - libpangocairo-1.0-0
  - libstdc++6
  - libx11-6
  - libx11-xcb1
  - libxcb1
  - libxcomposite1
  - libxcursor1
  - libxdamage1
  - libxext6
  - libxfixes3
  - libxi6
  - libxrandr2
  - libxrender1
  - libxss1
  - libxtst6
  - lsb-release
  - wget
  - xdg-utils
runcmd:
  - curl -sL https://deb.nodesource.com/setup_13.x | bash
  - apt install -y nodejs
  - [ cd, /home/ubuntu ]
  - [ git, clone, "${git_repo}" ]
  - [ cd, social-media-archive ]
  - [ git, checkout, "${git_commit}" ]
  - [ npm, install ]
  - [ systemctl, enable, --now, arsheev ]
write_files:
  - path: /etc/systemd/system/arsheev.service
    owner: root:root
    permissions: '0755'
    content: |
        [Unit]
        Description=Start Arsheev Worker
        Wants=network-online.target
        After=network-online.target

        [Service]
        Type=simple
        Environment=NODE_ENV=production
        Environment=PGSSLMODE=no-verify
        Environment=HEROKU_POSTGRESQL_OLIVE_URL=${postgres_url}
        Environment=BROWSERLESSIO_API_TOKEN=${browserless_key}
        Environment=REDISGREEN_URL=${redis_url}
        Environment=AWS_ACCESS_KEY_ID=${aws_access_key_id}
        Environment=AWS_SECRET_ACCESS_KEY=${aws_secret_access_key}
        Environment=HALT_ON_EMPTY_QUEUE=true
        WorkingDirectory=/home/ubuntu/social-media-archive
        StandardOutput=file:/var/log/arsheev-${worker}.out
        StandardError=file:/var/log/arsheev-${worker}.err
        ExecStart=/usr/bin/node /home/ubuntu/social-media-archive/worker/${worker}.js
        Restart=on-failure
        RestartSec=15

        [Install]
        WantedBy=multi-user.target