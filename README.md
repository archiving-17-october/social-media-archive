# Arsheev

Arsheev is the social media archival system. It was built during and in service of the October 17 Lebanese Revolution of 2019. It employs a variety of tactics to collect social media content, including images and videos, in a way that is efficient, cost-effective, and avoids the rate limits imposed by the APIs of the platforms in question.

# Status

The system works in a basic sense but it's still rough around the edges and not well documented. Deploying it for other applications may require forking this repository and making local changes. The collector still experiences a small number of failures that require manual intervention.

The current implementation supports Twitter, but a prototype of Instagram support is in the works.

# Resources

The system requires a few resources provisioned in data centers. Our implementation is split between Heroku and AWS, but you could take a different approach if you wanted to. Keep in mind that some of the tooling in this repository assumes publicly accessible secure Postgres and Redis connections which may or may not work with Amazon RDS or Amazon Elasticache.

## Database

We use a postgres database as our primary backing store. Important considerations are row and connection limit. Certain more affordable hosted postgres plans will impose limits on the number of rows they will store which could put a cap on the size of your archive. The connection limit constraints the scale of the collector (described below) as running social media post extraction in parallel will require a separate database connection per extractor instance.

We are currently running the [Heroku Standard 0 Postgres](https://elements.heroku.com/addons/heroku-postgresql) plan, which costs $50/month.

## Redis

Redis is used as the backing store for the collector's work queue. Important considerations here are available memory and connection limit. Managed redis plans very wildly in the properties they offer at the same price point. You are going to want something with a very high connection limit so that you can scale up your collector.

We are currently running the [Mini Development RedisGreen](https://elements.heroku.com/addons/redisgreen) plan, which costs $14/month.

## S3

We store images and videos on Amazon S3. There's no special configuration that we do here.

## Browserless.io

We use [browserless.io](https://browserless.io/) to host our headless browsers. They integrate seamlessly with [pupeteer](https://github.com/puppeteer/puppeteer) so there is no difference in the scraping code we write. This decision was made when Heroku was our sole platform and we were experiencing memory issues, but it can probably be relaxed now that we run our gather process on EC2.

# Overview

Arsheev it's made up of two components: a front-end and that allows people to view the archive using web browser and exposes a JSON API and a collector that scrapes social media content to populate the archive. Both components live in this repository.

## Database

### Record IDs

Social media posts are identified in the system by a tuple of their platform domain and their ID on that platform. Together the pair forms a globally unique identifier for that post. For example a tweet ID might be `twitter.com,1209477511530323968`. 

### Origins

To the best of our abilities we record the origin of every record in our database which attempts to capture the means by which it entered the archive. The origin will record what kind of gather or extract process caused us to find and record the record.

## Front-end

The front-end is a relatively straightforward web application that reads from the postgres database and S3 bucket. It is hosted on Heroku and launched with the `web` process in the `Procfile`. The entry point is `web/server.js`.

## Collector Pipeline

The collector is a set of scripts that run intermittently to scrape social media content and populate the database and S3 bucket that for the front-end. It runs on Amazon AWS EC2 instances provisioned with auto-scaling groups. Currently the groups are scaled up and down as needed manually, but eventually they will be on a schedule. The collector consists of three processes: gather, extract, and media, each described in detail below.

Each process is designed to be horizontally scalable and have an arbitrary number of instances running simultaneously, bounded only by cost and available resources. Each process is fed by a work queue, with a process often pushing work onto the queue of the next process in the pipeline. The use of a work queue makes it easy to restart jobs that may fail due to crashes and other errors, as well as giving us a nice set of semantics and guarantees with which to reason about how work moves through the system.

The collector originally ran on Heroku as well as the front end but was moved to EC2 for cost efficiency reasons.

### Gather Process

The goal of the gather process is to find social media IDs to push onto the extract queue as extract jobs. The entry point is `worker/gather.js`. The Twitter specific gather logic lives in `platform/twitter/gather.js`.

There are several types of gather jobs. The most basic one at the time of this writing is `twitter-hashtag-on-date`. It launches a headless browser and performs a Twitter search for a given hashtag on a given day and creates an extract job for of every tweet it finds on the extract queue.

The gather process is a little bit special in that it is the first step in the pipeline, so this is where recurring and one off jobs are pushed. For example, a recurring daily Twitter hashtag search will be pushed onto the gather queue once a day and when the collector wakes up they gather process will act on that job.

### Extract Process

The primary goal of the extract process is to extract all the textual information from a social media post and store it in the database. Additionally, if an extract process finds media associated with a social media post it will push the relevant information onto the media queue to be collected by the media process. An extract process might even push new extract jobs onto the extract queue if it finds internal links, for example tweets mentioning or replying to other tweets. The entry point is `worker/extract.js`. The Twitter specific extraction logic lives in `platform/twitter/extract.js`.

Extraction does not use a headless browser, but rather parses the page in memory using [cheerio](https://cheerio.js.org/). This means we can scale up our extractors a fair bit. We've managed to run 210+ extractor workers in parallel.

### Media Process

The goal of the media process is to download and potentially transcode media from the platforms into our S3 storage. In the cases of images and some videos this is a simple case of copying a file from a URL discovered by the extract process to a location in our S3 bucket. In the case of most of the video on Twitter, however, the situation is more complicated as the videos are not available in a convenient format and we need to transcode them into MP4s ourselves. This is the most resource intensive part of the pipeline. It requires FFMpeg to be installed on the system to function. The entry point is `worker/media.js` and relevant files are `platform/twitter/video` (for extracting video and playlist files from IDs discovered through extraction) and `storage/s3.js` (where the transcoding logic lives, though that should probably be refactored out of the s3 file).

## Deployment

In order to make the process repeatable we use [terraform](https://www.terraform.io/) to describe and deploy our infrastructure on AWS. We don't use their cloud service and only use their free command line tools. The launch configurations and auto-scaling groups that make up the collector are described in `infrastructure/infrastructure.tf`. Running `terraform apply` in the `infrastructure` folder will deploy the collector infrastructure to AWS.

The collector instances do the following on boot:

1. Install node and FFmpeg
2. Clone the repository to a predictable location
3. Install a systemd service to run the relevant worker script with all the relevant environment variables set
4. Immediately runs the system service

This is described in `infrastructure/cloud-init.tpl`.

## Usage

There currently is no interface to push work to the gather queue. Instead we use one-off scripts that we run locally on our developer machines. It is a high priority of ours to get past this.

Once deployed, the collector can be started by setting the auto-scaling groups instance count to a value greater than zero. Amazon will then add EC2 instances to your groups running the cloud-init described above on each one. The workers should start consuming jobs off of their relevant queues. Once everything is processed you can stop the collector by setting the autoscaling group instance counts to zero. Amazon will then kill your EC2 instances.

Down the line we expect to run this on a schedule, which is something autoscaling groups explicitly support.
