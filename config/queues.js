require('dotenv').config();
const { timeout } = require('../common')
const Queue = require('bull');
const YAML = require('yaml')
var Redis = require('ioredis')

const REDIS_URL = process.env.NODE_ENV === 'production' ? `${process.env.REDISGREEN_URL}1` : 'redis://127.0.0.1:6379'
var client = new Redis(REDIS_URL);
var subscriber = new Redis(REDIS_URL);

// gather workers will not run if we're using more than QUEUE_MEMORY_THRESOLD
// of memory on our redis instance. this is to avoid the costly and chaotic
// automatic upgrades that happens when we hit 90% memory usage on redisgreen.
const QUEUE_MEMORY_THRESOLD = 0.75

// timeout in ms to wait for memory usage to go down
const QUEUE_MEMORY_TIMEOUT = 15000

const QUEUE_MEMORY_CHECK_FREQUENCY = 1000

var bullOptions = {
    createClient: function (type) {
      switch (type) {
        case 'client':
          return client;
        case 'subscriber':
          return subscriber;
        default:
          return new Redis(REDIS_URL);
      }
    },
    redis: {
      reconnectOnError(err) {
        if (err.message.includes("ETIMEDOUT")) {
          console.log("[redis] ETIMEDOUT, reconnecting")
          return true;
        }
      },
    },
    defaultJobOptions: {
      attempts: 8,
      removeOnComplete:true
    }
  }


let _gather, _extract, _commit, _media;

let queues = {
    get gather() {
        if(!_gather) _gather = new Queue("gather", bullOptions)
        return _gather;
    },
    get extract() {
        if(!_extract) _extract = new Queue("extract", bullOptions)
        return _extract;
    },
    get commit() {
        if(!_commit) _commit = new Queue("commit", bullOptions)
        return _commit;
    },
    get media() {
        if(!_media) _media = new Queue("media", bullOptions)
        return _media;
    }
}

async function status() {
    let gather = (await queues.gather.getWaitingCount()) + (await queues.gather.getActiveCount())
    let extract = (await queues.extract.getWaitingCount()) + (await queues.extract.getActiveCount())
    let media = (await queues.media.getWaitingCount()) + (await queues.media.getActiveCount())

    return { pending: { gather, extract, media } }
}

function redisInfo() {
  return new Promise((resolve, reject) => {
      client.sendCommand(
          new Redis.Command( 'info', ['memory'], 'utf-8', 
              function(err,value) {
                if (err) {
                    reject(err);
                } else {
                    let payload = value.toString()
                    resolve(YAML.parse(payload.replace(/:/g, ": ")))
                }
              }
          )
      );
  })
}

var memoryCheckCountDown = QUEUE_MEMORY_CHECK_FREQUENCY;

async function enqueue(queue, job) {
  if(memoryCheckCountDown-- <= 0) {
    console.log('[enqueue]', 'memory check');
    memoryCheckCountDown = QUEUE_MEMORY_CHECK_FREQUENCY;
    let highMemoryUsage = true
    while(highMemoryUsage) {
        let { used_memory, maxmemory } = await redisInfo()
        let usage = used_memory / maxmemory
        highMemoryUsage = maxmemory > 0 && usage > QUEUE_MEMORY_THRESOLD
        console.log('[enqueue]', 'used', used_memory, 'max', maxmemory, 'percent', usage, 'threshold', QUEUE_MEMORY_THRESOLD);
        if(highMemoryUsage) {
            console.log('[enqueue]', `memory usage too high, waiting`);
            await timeout(QUEUE_MEMORY_TIMEOUT)
        }
    }

    console.log('[enqueue]', 'ok');
  }

  await queue.add(job)
}


module.exports = { queues, status, redisInfo, enqueue }