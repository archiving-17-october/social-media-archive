const AWS = require('aws-sdk');
AWS.config.update({ region: 'us-east-1' });
const ec2 = new AWS.EC2({ apiVersion: '2016-11-15' });

var params = {
    Filters: [ { Name: "tag:WorkerInstance",
                 Values: [ "true" ] } ]
}

ec2.describeInstances(params, (err, data) => {
    if (err) throw err;
    let InstanceIds = data.Reservations
                        .map(r => r.Instances[0])
                        .filter(i => i.State.Name==='stopped')
                        .map(i => i.InstanceId)
    console.log('starting ', InstanceIds);
    
    ec2.startInstances({InstanceIds}, (err, data) => {
        if(err) throw err;
        console.log('ok');
        process.exit(0)
    })
});