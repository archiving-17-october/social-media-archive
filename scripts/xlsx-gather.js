const { queues } = require('../config/queues');
const url = process.argv[2]
console.log('importing', url);

(async _ => {
    await queues.gather.add({type:'lebanonprotests.com-xlsx-import', url})
    process.exit(0)
})()